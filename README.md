# do_boot.sh

URL: https://gist.github.com/leucos/6f8d93de3493431acd29

## Notes

This method does not use Terraform.  Instead, it uses native Ansible and bash to read a user-defined inventory file and create the defined Droplets on DigitalOcean.  It does this in parallel to speed up processing.  The original script had to be modified slightly to work.

## Benefits

* Significant reduction in complexity gained by taking Terraform out of the picture.
* Intuitive configuration.  No need to translate between Terraform and Ansible terminology and concepts.
* Hosts are defined in easy-to-read, native Ansible inventory format.
* Since it uses bash, it might be easier to modify to add new features in the future.
* Can work with different inventories by pointing the script at new directories.
* Users can read the inventory directly from the inventory directory.

## Drawbacks

* While this makes targeting hosts easy, it makes displaying information more difficult. Ansible roles for displaying basic IP address information are required for each play.
* No concept of tags.
* Future features are going to have to be added in-house.
* Uses name-based matching to determine if Droplets exist.  This could maybe(?) pick up hosts that aren't supposed to be involved if there's a naming collision.

## Requirements

1. Install Ansible
2. Install `jq`
3. Export `DIGITALOCEAN_TOKEN` and `DO_API_TOKEN`, each set to a DigitalOcean token with R/W access
4. Edit the `do_boot.sh` script to set `DEFAULT_KEY` to a comma-separated list of SSH key IDs (as given by the DO API)
5. Edit the `do_boot.sh` script to set any other defaults by editing the values of the other `DEFAULT_*` variables.

## Usage

1. In the `./inventories` directory, add your infrastructure requirements to the `hosts` file using native Ansible inventory syntax.  You can group hosts arbitrarily.

   The default parameters for creating Droplets can be overridden on a per-Droplet basis by using the following parameters:

   * `do_size_slug`
   * `do_region_slug`
   * `do_image_slug`
   * `do_key`

   For example, you could create an inventory like this:

   ```
   [web]
   web1
   web2

   [db]
   db1 do_size_slug="2gb"
   ```

   This would define three Droplets in two groups.  The `web1` and `web2` Droplets belong to the `web` group and are created with the default settings.  The `db1` Droplet belongs to the `db` group and will use a 2GB Droplet instead of the default.

2. Create the Droplet resources by going back to the main directory and typing: `./do_boot.sh inventories`

3. In Ansible, run your ad-hoc commands or playbooks against the generated resources.  Use normal Ansible targetting to select hosts.  Your original inventory file and the companion `./inventories/generated` file are the only things impacting the host definitions.

4. To destroy infrastructure, call the script again with the word `deleted` as a final parameter, like `./do_boot.sh inventories deleted`.
